# DevOps Task 2 CI Configuration

#### gitlab-runner Registration Using docker exec

```bash
docker exec -it [CONTAINER_NAME] gitlab-runner register \
--non-interactive \
--url http://gitlab \
--registration-token [TOKEN] \
--executor docker \
--docker-image alpine:latest \
--docker-network-mode gitlab-net
```
